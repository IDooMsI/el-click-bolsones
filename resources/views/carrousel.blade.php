@extends('googleform')
@section('carrousel')
<h2 class="text-center titulocarrusel">¿POR QUÉ HACEMOS EL CLICK?</h2>
<div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
  <ol class="carousel-indicators carruselindicadores">
    <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-2" data-slide-to="1"></li>
    <li data-target="#carousel-example-2" data-slide-to="2"></li>
    <li data-target="#carousel-example-2" data-slide-to="3"></li>

  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <div class="view">
        <img class="imgcarruselmobile w-100" src="{{ asset('storage/images/Mobile-ImagenesSlide/El-Click-Web-Mobile-ImagenesSlide-01.jpg') }}">
        <img class="imgcarruseltablet w-100" src="{{ asset('storage/images/Desktop-ImagenesSlide/El-Click-Web-Desktop-ImagenesSlide-01.jpg') }}">
        <img class="imgcarruseldesk w-100" src="{{ asset('storage/images/Desktop-ImagenesSlide/El-Click-Web-Desktop-ImagenesSlide-01.jpg') }}">
        <div class="mask rgba-black-light"></div>
      </div>
      <div class="carousel-caption">
        <h1 class="h3-responsive h3carrusel margenh3carr">COMEMOS MÁS</h1>
        <h1 class="h3-responsive h3carrusel margenh3carr">SANO Y RICO</h1>
        <hr class="bordecarrusel"></hr>
        <p class="parrafocarrusel text-center"> Las frutas y verduras libres de agrotóxicos tienen doble beneficio: mantienen todos sus nutrientes y su sabor real. ¡Trabajamos con alimentos recién cosechados! </p>
      </div>
    </div>
    <div class="carousel-item">
      <div class="view">
        <img class="imgcarruselmobile w-100" src="{{ asset('storage/images/Mobile-ImagenesSlide/El-Click-Web-Mobile-ImagenesSlide-02.jpg') }}">
        <img class="imgcarruseltablet w-100" src="{{ asset('storage/images/Desktop-ImagenesSlide/El-Click-Web-Desktop-ImagenesSlide-02.jpg') }}">
        <img class="imgcarruseldesk w-100" src="{{ asset('storage/images/Desktop-ImagenesSlide/El-Click-Web-Desktop-ImagenesSlide-02.jpg') }}">
        <div class="mask rgba-black-strong"></div>
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive h3carrusel 2-line mb-0">MEJORAMOS EL TRABAJO</h3>
        <h3 class="h3-responsive h3carrusel 2-line">DE PEQUEÑXS PRODUCTORXS</h3>
        <hr class="bordecarrusel"></hr>
        <p class="parrafocarrusel text-center"> La agroecología y el sistema de bolsones son una oportunidad real de trabajo digno y sano para más personas. ¡El 60% del valor del bolsón va directo a las familias productoras! </p>
      </div>
    </div>
    <div class="carousel-item">
      <div class="view">
        <img class="imgcarruselmobile w-100" src="{{ asset('storage/images/Mobile-ImagenesSlide/El-Click-Web-Mobile-ImagenesSlide-03.jpg') }}">
        <img class="imgcarruseltablet w-100" src="{{ asset('storage/images/Desktop-ImagenesSlide/El-Click-Web-Desktop-ImagenesSlide-03.jpg') }}">
        <img class="imgcarruseldesk w-100" src="{{ asset('storage/images/Desktop-ImagenesSlide/El-Click-Web-Desktop-ImagenesSlide-03.jpg') }}">
        <div class="mask rgba-black-slight"></div>
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive h3carrusel margenh3carr">CAMBIAMOS HÁBITOS</h3>
        <h3 class="h3-responsive h3carrusel margenh3carr">DESDE EL CONSUMO</h3>
        <hr class="bordecarrusel"></hr>
        <p class="parrafocarrusel text-center"> Buscamos ser una alternativa al mercado convencional de producción y desde un compromiso colectivo podemos lograr acceder a alimentos más sanos y a un mejor precio. </p>
      </div>
    </div>
    <div class="carousel-item">
      <div class="view">
        <img class="imgcarruselmobile w-100" src="{{ asset('storage/images/Mobile-ImagenesSlide/El-Click-Web-Mobile-ImagenesSlide-04.jpg') }}">
        <img class="imgcarruseltablet w-100" src="{{ asset('storage/images/Desktop-ImagenesSlide/El-Click-Web-Desktop-ImagenesSlide-04.jpg') }}">
        <img class="imgcarruseldesk w-100" src="{{ asset('storage/images/Desktop-ImagenesSlide/El-Click-Web-Desktop-ImagenesSlide-04.jpg') }}">
        <div class="mask rgba-black-slight"></div>
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive h3carrusel 2line margenh3carr"> INCENTIVAMOS EL </h3>
        <h3 class="h3-responsive h3carrusel 2line margenh3carr"> RESPETO POR LA TIERRA</h3>
        <hr class="bordecarrusel"></hr>
        <p class="parrafocarrusel text-center"> En momentos de emergencia climática creemos que la agroecología es el camino. Una forma de cultivar que convive con el ecosistema en vez de destruirlo. </p>
      </div>
    </div>
  </div>

</div>

<div class="carruselseparador">
</div>



    @yield('contact')
@endsection