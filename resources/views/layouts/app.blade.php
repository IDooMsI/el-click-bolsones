<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>El Click</title>
    <!-- Pop-up JS -->
    <script src="/js/pop-up.js" defer></script>
    
    <!-- Section JS -->
    <script src="/js/section.js" defer></script>

    <!-- Bootstrap CSS -->
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> --}}
    <!-- Iconos -->
    <script src="https://kit.fontawesome.com/4baeadfaf1.js" crossorigin="anonymous"></script>

    {{-- <script src="https://unpkg.com/scrollreveal"></script> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    {{-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@600;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;900&display=swap" rel="stylesheet"> --}}

    <!-- Styles -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet"> 
    <!-- Styles -->
    <link href="{{ asset('/css/pop-up.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/section.css') }}" rel="stylesheet"> 

</head>
<body>
  <div class="pop-up__container" id="pop-up__container"></div>
  <div class="preloader-wrapper">
    <div class="preloader"></div>
  </div>

      

    <div id="app">
      <a href="https://api.whatsapp.com/send?phone=5491136718145&text=" target="_blank" class="float">
       <i class="fab fa-whatsapp my-float"></i>
      </a>
      <div class="label-container">
       <div class="label-text">Hola! Consultános lo que precises y te responderemos a la brevedad.</div>
       <i class="fa fa-play label-arrow"></i>
      </div>
        {{-- Barra de navegacion --}}
        <nav class="navbar navbar-expand-lg navbar-light bg-light shadow headline">
            <a class="navbar-brand" href="#">
                <img src="{{ asset('/images/mobile/El Click-Web-Mobile_LOGO.svg') }}" width="80" height="70" alt="logo el click">
            </a>
            <div class="text-center titulonav">
                <h4 class="navbar-text">
                  <div class="tagline h4barranav">FRUTAS Y VERDURAS LIBRES DE AGROTÓXICOS</div>
                </h4>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
              <ul class="navbar-nav text-center justify-content-end mt-2 mt-lg-0">
                <li class="nav-item">
                  <a class="nav-link" href="#bolson">El bolsón</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#faqs">Info importante</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#gform">Pedílo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#dform">Contacto</a>
                  </li>
              </ul>
            </div>
        </nav>
        <main>
            @yield('content')
        </main>
        <footer>
            <div class="part-footer f-l">
                <h3>¡SEGUINOS!</h3>
            </div>
            <div class="part-footer f-m">
                <div class="social-media">
                    <a href="https://www.instagram.com/elclickbolsones/"><i class="fab fa-instagram"></i></a>
                    <a href="https://www.facebook.com/elclickbolsones/"><i class="fab fa-facebook-f"></i></a>
                    <a href="#"><i class="fab fa-whatsapp"></i></a>
                </div>
            </div>
        </footer>
    </div>

    @stack('scripts')
    <!-- Scripts -->
    <script src="{{ mix('/js/app.js') }}"></script>
    
</body>
</html>
