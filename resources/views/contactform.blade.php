@extends('carrousel')
@section('contact')

    <div class="container-fluid">
        <div class="row contact-form">
            <div class="col-md-12 col-lg-6">
                <p id="dform"></p>
                <div class="title text-center mx-auto">
                    <h2>CONTACTO</h2>
                </div>
                <div class="sub-title text-center mx-auto">
                    <p>Escribinos y te responderemos a la brevedad</p>
                <img src="{{ asset('storage/images/Iconos/Icono-08.svg') }}" alt="">
                </div>
                <div class="formulario">
                <form id="contactForm" action="{{ route('email') }}" method="POST">
                    @csrf
                        <div class="form-group row">
                        <div class="col-sm-6 col-md-8 col-lg-10 mx-auto">
                            <input type="text" name="nombre" class="form-control rounded-pill" id="inputText" placeholder="Nombre" required>
                        </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-sm-6 col-md-8 col-lg-10 mx-auto">
                            <input type="email" name="email" class="form-control rounded-pill" id="inputEmail" placeholder="Mail" required>
                        </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 col-md-8 col-lg-10 mx-auto">
                                <textarea class="form-control" name="mensaje" id="FormControlTextarea" rows="10" placeholder="Mensaje"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                        <div class="mx-auto">
                            <button type="submit" id="btnEnviar" class="btn btn-success rounded-pill" onclick="sendEmail()">Enviar</button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="img-form col-6"></div>
        </div>    
    </div>
@endsection
