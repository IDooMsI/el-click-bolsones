@extends('faqs')
@section('google')
    <div id="gform" class="google-form">
        <div class="title-form text-center mx-auto">
            <h2>COMPLETÁ EL FORMULARIO Y TU PEDIDO QUEDARÁ CONFIRMADO</h2>
        </div>
        <div class="formulario justify-content-center">
            <iframe id="google-form" class="w-100" src="https://docs.google.com/forms/d/e/1FAIpQLScJbsGNlCqwNAi2H2EvgDcYdUiOkimnMiNqFr8RmCLBAEzJew/viewform?embedded=true">Cargando…</iframe>
        </div>
    </div>
    @yield('carrousel')    
@stop
@section('scripts')
@stop