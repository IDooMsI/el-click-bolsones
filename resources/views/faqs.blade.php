@extends('content')
@section('faqs')
<div class="row divfaqs">
  <div class="section__container row mx-0" id="section__container"></div>
</div>



<div class="row divfaqs">
        <div class="accordion col-12 col-md-6" id="accordionExample">
            <div class="card panelizq">
                <div class="card-header" id="headingOne1">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="false" aria-controls="collapseOne1">
                            <h5 class="mb-0">
                                <img class="img" src="{{ asset('storage/images/Iconos/Icono-02.svg') }}">
                                ¿Cómo pido mi bolsón? <i class="iconoflechafaq float-right fas fa-angle-right rotate-icon"></i>
                            </h5>
                        </button>
                    </h2>
                </div>
                <div id="collapseOne1" class="collapse" aria-labelledby="headingOne1" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul>
                            <li>Pedír tu bolsón es MUY FÁCIL. Completás el formulario <a href="https://docs.google.com/forms/d/e/1FAIpQLSeTSkXkMeX_EGXJFY7H_E4b3uCW4DPpno_wpasilHu2kosMvA/viewform?usp=send_form" target="_blank" style="color: #176301;">haciendo click aquí y ahora</a> y tu pedido queda automáticamente CONFIRMADO.</li>
                            <br>
                            <li>Lo abonás en efectivo al retirarlo, el día LUNES o MARTES, dependiendo del punto de retiro que elijas.</li>
                            <br>
                            <li>El listado de puntos de retiro está detallado en las siguientes solapas. Entregamos todas las semanas en más de 60 puntos en el AMBA.</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card panelizq">
                <div class="card-header" id="headingTres">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseTres" aria-expanded="false" aria-controls="collapseTres">
                            <h5 class="mb-0">
                                <img class="img" src="{{ asset('storage/images/Iconos/Icono-03.svg') }}">
                                ¿Cómo son los bolsones y las bolsitas? <i class="iconoflechafaq float-right fas fa-angle-right rotate-icon"></i>
                            </h5>
                        </button>
                    </h2>
                </div>
                <div id="collapseTres" class="collapse" aria-labelledby="headingTres" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul>
                            <li>Los bolsones traen alrededor de 8 kgs de frutas y verduras libres de agrotóxicos, de estación, recién cosechadas y directo de familias productoras. El valor es de 1100 pesos y vienen entre 10 y 12 variedades diferentes cada semana!.</li>
                            <br>
                            <li>Todas las semanas hay distintas opciones agroecológicas para sumar al pedido y completar tu dieta a medida. Tiene un valor de $200 por variedad. </li>
                            <br>
                            <li>También podés agregar desde este formulario a tu pedido MIEL, VINO, YERBA y HUEVOS libres de agrotóxicos!</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card panelizq">
                <div class="card-header" id="headingCinco">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseCinco" aria-expanded="false" aria-controls="collapseCinco">
                            <h5 class="mb-0">
                                <img class="img" src="{{ asset('storage/images/Iconos/Icono-01.svg') }}">
                                Leer antes de pedir <i class="iconoflechafaq float-right fas fa-angle-right rotate-icon"></i>
                            </h5>
                        </button>
                    </h2>
                </div>
                <div id="collapseCinco" class="collapse" aria-labelledby="headingCinco" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul>
                            <li>Si ya pediste 1 bolsón y querés agregar a tu pedido otro bolsón, bolsitas, o algún artículo extra, escribinos por privado y te lo agregamos! No envíes un nuevo pedido por el formulario ;)</li>
                            <br>
                            <li>Alguna variedad del bolsón puede llegar a modificarse eventualmente por cuestiones climáticas o de cosechas, en esos casos será reemplazada siempre por una nueva variedad o más cantidad de las otras variedades!</li>
                            <br>
                            <li>Los bolsones en los que entregamos las frutas y verduras, son RETORNABLES para ser reutilizados y así reducir la cantidad de plástico. El valor es de $50 y solo se cobra la primera vez que buscás el bolsón o cuándo te olvidas de devolverlo ;)</li>
                        </ul>
                        <p>Por cualquier otra consulta escribinos por INBOX a nuestro Facebook, Instagram, a nuestro mail consumidores@elclickbolsones.com  o un whatsapp al 1136718145.</p>
                    </div>
                </div>
            </div>
        </div>

        {{-- Punto de retiro --}}
        <div class="accordion col-12 col-md-6" id="accordionExample">
            <div class="card panelder">
                <div class="card-header" role="tab" id="headingTwo2">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                            <h5 class="mb-0">
                                <img class="img" src="{{ asset('storage/images/Iconos/Icono-04.svg') }}">
                                Puntos de retiro <span class="spanfaq">(Lunes)</span> <i class="iconoflechafaq float-right fas fa-angle-right rotate-icon"></i>
                            </h5>
                        </button>
                    </h2>
                </div>
                <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2" data-parent="#accordionExample">
                    <div class="card-body">
                        <p><strong>Agronomía </strong>/ Av. Constituyentes 4088<br>De 15:30 a 19:30 hs, Dietética Shizen.</p>
                        <p><strong>Almagro Rivadavia Loria </strong>/ Av Rivadavia 3593<br>De 16 a 19 hs, Almacén Loria.</p>
                        <p><strong>Almagro Rivadavia Rawson </strong>/ Av Rivadavia 4205<br>De 16:30 a 19 hs, Dietética Peperina.</p>
                        <p><strong>Beccar </strong>/ Av. Rolón 2302<br>De 16:30 a 19 hs, Peperina.</p>
                        <p><strong>Boedo </strong>/ Av San Juan 3552<br>De 16:00 a 19 hs, Oxum vida Sana.</p>
                        <p><strong>Castelar </strong>/ San Nicolás 710<br>De 16:30 a 19 hs, El Click.</p>
                        <p><strong>Congreso </strong>/ Entre Rios 188<br>De 15 a 19 hs, El nombre del viento.</p>
                        <p><strong>Don Torcuato </strong>/  Ángel T. de Alvear 753<br>De 16:30 a 19 hs, Menta y jengibre almacén.</p>
                        <p><strong>Flores </strong>/ Av Carabobo 320<br>De 14 a 19 hs, Corazón de Nuez.</p>
                        <p><strong>Floresta </strong>/ Av Rivadavia 8869<br>De 16 a 19 hs, Ola Veggie.</p>
                        <p><strong>Florida Oeste </strong>/ Martín Haedo 3418<br>De 15:30 a 19 hs, El Click.</p>
                        <p><strong>Ituzaingó </strong>/ Lavalleja 380<br>De 17:30 a 19 hs, Pensando en vos.</p>
                        <p><strong>Liniers </strong>/ Av Emilio Castro 7441<br>De 15 a 19 hs, Almacen dietetico Ayllu.</p>
                        <p><strong>Los Polvorines </strong>/ Av Pte Perón 3520.<br>De 15 a 19 hs, BioLife.</p>
                        <p><strong>Martínez Yrigoyen </strong>/ Hipólito Yrigoyen 1788<br>De 11:30 a 16 hs, Semilla Amarilla.</p>
                        <p><strong>Mataderos </strong>/ Av Alberdi 6377<br>De 16 a 19 hs, Brisa Natural.</p>
                        <p><strong>Munro </strong>/ Mitre 2405<br>De 15 a 19 hs, Dietética Amaranto.</p>
                        <p><strong>Olivos Villate </strong>/ Carlos Villate 2348<br>De 11 a 13 hs y de 16 a 19 hs, Lawen Almacén Natural.</p>
                        <p><strong>Parque Centenario Díaz Vélez </strong>/ Díaz Vélez 5151<br>De 14:30 a 19 hs, Dietética Simple & Natural.</p>
                        <p><strong>Parque Chacabuco </strong>/ Av Directorio 1096<br>De 14:30 a 19 hs, Almacén Canella.</p>
                        <p><strong>Paternal </strong>/ Av San Martín 1510<br>De 14 a 19 hs, Vitalcer.</p>
                        <p><strong>Ramos Mejía </strong>/ Av de Mayo 720<br>De 16:30 a 19 hs, Sano y Natural.</p>
                        <p><strong>Saavedra </strong>/ Plaza 3999<br>De 15:30 a 19 hs, Dietética Benidorm.</p>
                        <p><strong>San Fernando </strong>/ Constitución 777<br>De 14 a 19 hs, Vitalcer.</p>
                        <p><strong>San Miguel </strong>/ Paunero 1312<br>De 15 a 19 hs, BioLife.</p>
                        <p><strong>Tigre </strong>/ Av Cazón 1455<br>De 14 a 18 hs, Verde Gratitud.</p>
                        <p><strong>Victoria </strong>/ Santamarina 1294<br>De 13 a 19 hs, Dietética Tomy.</p>
                        <p><strong>Villa Adelina </strong>/ Av. de Mayo 534<br>De 15:30 a 19 hs, Vivir Natural.</p>
                        <p><strong>Villa Ballester </strong>/ Pueyrredón 2456<br>De 14:30 a 19 hs, Dietética El Bolsón.</p>
                        <p><strong>Villa Crespo Gallardo </strong>/ Av Ángel Gallardo 74<br>De 16:30 a 19 hs, El Click.</p>
                        <p><strong>Villa Devoto Lope de Vega </strong>/ Av Lope de Vega 3473<br>De 15 a 19 hs, Dietética Rojas.</p>
                        <p><strong>Villa Devoto Gutenberg </strong>/ Gutenberg 3774<br>De 15 a 18 hs, Mercado Cannavata.</p>
                        <p><strong>Villa Devoto Nogoyá </strong>/ Nogoyá 4490<br>De 15 a 19 hs, Adzuki.</p>
                        <p><strong>Villa Luro </strong>/ Albariño 785<br>De 17:30 a 19 hs, El Limonero.</p>
                        <p><strong>Villa Ortuzar </strong>/ Av Álvarez Thomas 1579<br>De 16:30 a 19 hs, Dietética Nueva Era.</p>
                        <p><strong>Villa Santa Rita Nazca </strong>/ Nazca 1111<br>De 15 a 19 hs, Cilantro Mercado.</p>
                        <p><strong>Villa Urquiza Triunvirato </strong>/ Av Triunvirato 4202<br>De 16:30 a 19 hs, Biga.</p>
                    </div>
                </div>
            </div>

            <div class="card panelder">
                <div class="card-header" id="headingCuatro">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseCuatro" aria-expanded="false" aria-controls="collapseCuatro">
                            <h5 class="mb-0">
                                <img class="img" src="{{ asset('storage/images/Iconos/Icono-04.svg') }}">
                                Puntos de retiro <span class="spanfaq">(Martes)</span> <i class="iconoflechafaq float-right fas fa-angle-right rotate-icon"></i>
                            </h5>
                        </button>
                    </h2>
                </div>
                <div id="collapseCuatro" class="collapse" aria-labelledby="headingCuatro" data-parent="#accordionExample">
                    <div class="card-body">
                        <p><strong>Almagro Corrientes </strong>/ Av Corrientes 4360<br>De 14:30 a 19 hs, Gingko Diet.</p>
                        <p><strong>Avellaneda </strong>/ Estanislao Zeballos 698<br>De 9 a 18 hs, Siempre verde.</p>
                        <p><strong>Barrio Norte </strong>/ Sanchez de Bustamante 1178<br>De 14:30 a 18 hs, Dietética Majula.</p>
                        <p><strong>Belgrano Crámer </strong>/ Av Crámer 2594<br>De 14:30 a 19:00 hs, A granel.</p>
                        <p><strong>Belgrano Juramento </strong>/ Juramento 2145<br>De 14 a 19 hs, Vitalcer</p>
                        <p><strong>Boulogne </strong>/ Serrano 2366<br>De 16 a 19, Almacén Nuevo Mundo.</p>
                        <p><strong>Caballito </strong>/ Av Avellaneda 1786<br>De 9 a 14 y 16 a 19 hs, Sereal Nutrición.</p>
                        <p><strong>Caseros </strong>/ J B Alberdi 4705<br>De 9 a 14 y 16 a 18 hs / Aflora.</p>
                        <p><strong>Ciudad Jardín </strong>/ Aviador Agnetta 2529<br>De 15:30 a 19 hs, El Click.</p>
                        <p><strong>Coghlan </strong>/ Iberá 3852<br>De 16:30 a 19 hs, Almacén Plutarco.</p>
                        <p><strong>Colegiales </strong>/ Álvarez Thomas 1029<br>De 12 a 19 hs, Tres Pimientas.</p>
                        <p><strong>Colegiales </strong>/ Amenábar 1150<br>De 12:30 a 14 hs y 16:30 a 19 hs, Nutri-Bas.</p>
                        <p><strong>Florida Oeste </strong>/ Martín Haedo 3418<br>De 15:30 a 19 hs, El Click.</p>
                        <p><strong>Haedo </strong>/ Esmeralda 458<br>De 15:30 a 19 hs, El Click.</p>
                        <p><strong>Hurlingham </strong>/ Albarracin 1044<br>De 10 a 18 hs, Amor y Pan.</p>
                        <p><strong>Las Cañitas </strong>/ Soldado de la independencia 1137<br>De 13 a 19 hs, Mamasara.</p>
                        <p><strong>Martinez </strong>/ Vieytes 602<br>De 10 a 19 hs, Almaserena.</p>
                        <p><strong>Monte Castro </strong>/ Miranda 4514<br>De 11 a 19 hs, Lazzarina's Almacén.</p>
                        <p><strong>Olivos </strong>/ Av Maipú 2693<br>De 11 a 18 hs, Vitalcer.</p>
                        <p><strong>Once </strong>/ Av Córdoba 2470<br>De 15:30 a 19 hs, Nativus.</p>
                        <p><strong>Palermo Hollywood </strong>/ Cabrera 6077<br>De 13 a 19 hs, Estilo Tila.</p>
                        <p><strong>Palermo Scalabrini </strong>/ Av Scalabrini Ortiz 2715<br>De 14 a 19 hs, A granel.</p>
                        <p><strong>Palermo Soho </strong>/ Paraguay 4393<br>De 15 a 19 hs, Franca.</p>
                        <p><strong>Palermo Soler </strong>/ Soler 3810<br>De 15 a 19 hs, El Click.</p>
                        <p><strong>Parque Centenario Aranguren </strong>/ J.F. Aranguren 213<br>De 14:30 a 19 hs, Casa Jache.</p>
                        <p><strong>Recoleta Anchorena </strong>/ Anchorena 1540<br>De 15 a 19 hs, La despensa.</p>
                        <p><strong>San Andrés </strong>/ Intendente Alvear 2731<br>De 16 a 19 hs, Semillero Ferreyra.</p>
                        <p><strong>San Isidro Centro </strong>/ Belgrano 240<br>De 11 a 19 hs, Vitalcer.</p>
                        <p><strong>San Isidro Lomas </strong>/ Don Bosco 1605<br>De 9 a 15 hs, Lo de Mauri.</p>
                        <p><strong>San Martín </strong>/ Juarez 4852<br>17 a 19 hs, Alma Zen Om.</p>
                        <p><strong>San Telmo </strong>/ Perú 1109<br>De 12:30 a 18 hs, Vitalcer.</p>
                        <p><strong>Vicente Lopez </strong>/ Laprida 1505<br>De 10 a 16 hs, La Hojarasca.</p>
                        <p><strong>Villa Bosch </strong>/ J M Bosch 825<br>De 15:30 a 19 hs, El Click.</p>
                        <p><strong>Villa Crespo </strong>/ Av Corrientes 5547<br>De 14 a 19 hs, Vitalcer.</p>
                        <p><strong>Villa del Parque </strong>/ Nazarré 3254<br>De 11 a 18 hs, Dietética Nazarre.</p>
                        <p><strong>Villa Pueyrredón </strong>/ Av. Mosconi 2794<br>De 11 a 13:30 y 17 a 20 hs, Il Contadino.</p>
                        <p><strong>Villa Santa Rita </strong>/ Emilio Lamarca 1999<br>De 12 a 14 y de 16 a 19 hs, Fers Almacén.</p>
                        <p><strong>Villa Urquiza </strong>/ Quesada 5094<br>De 16:30 a 19 hs, Dame tu mano.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@yield('google')
@stop 
@section('scripts')
@stop