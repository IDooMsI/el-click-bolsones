@extends('layouts.app')
@section('content')
    <section class="contenido" id="bolson">
        <img alt="banner-primera-seccion" id="banner" class="banner">
        <div class="boton">
            <a href="#gform"><button type="button" class="bc1 btn btn-success" id="boton">PEDÍLO ACÁ</button></a>
        </div>    
       <div class="ancla"><p id="faqs"></p></div>
    </section>
    @yield('faqs')
@endsection