@extends('layouts.admin')
@section('content')
    <form action="{{ route('bolson.store') }}" method="post" enctype="multipart/form-data" class="formadmin">
        @csrf
        @method('post')
        <div class="row">
            <div class="form-group col-4 col-lg-3 mx-auto text-center">    
                <label class="h3" for="">Imagen Mobile</label>
                <input name="mobile" type="file" class="form-control-file">
            </div>
            <div class="form-group col-4 col-lg-3 mx-auto text-center">    
                <label class="h3" for="">Imagen Tablet</label>
                <input name="tablet" type="file" class="form-control-file">
            </div>
            <div class="form-group col-4 col-lg-3 mx-auto text-center">    
                <label class="h3" for="">Imagen Desktop</label>
                <input name="desktop" type="file" class="form-control-file">
            </div>
        </div>
        <div class="col-6 mx-auto text-center">
            <button class="btn btn-success" type="submit">S u b i r</button>
        </div>
    </form>
@endsection