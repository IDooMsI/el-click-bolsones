@extends('layouts.admin')
@section('content')
    <div class="col-6 mx-auto text-center divcambiar fuenteadmin">
        <a href="{{ route('bolson.create') }}"><button class="btn btn-info">Cambiar imagen</button></a>
    </div>
    @if (isset($bolson))
    <div class="row mt-5 fuenteadmin">
        <div class="col-12 text-center">
            <h2>Ultimo bolson</h2>
        </div>
        <div class="col-4 col-lg-3 mx-auto text-center">
            <h3>Imagen Mobile</h3>
            <img class="w-100" src="{{ asset('storage/'.$bolson->image_mobile) }}" alt="ultimo bolson">
        </div>
        <div class="col-4 col-lg-3 mx-auto text-center">
            <h3>Imagen Tablet</h3>
            <img class="w-100" src="{{ asset('storage/'.$bolson->image_tablet) }}" alt="ultimo bolson">
        </div>
        <div class="col-4 col-lg-3 mx-auto text-center">
            <h3>Imagen Desktop</h3>
            <img class="w-100" src="{{ asset('storage/'.$bolson->image_desktop) }}" alt="ultimo bolson">
        </div>
    </div>
    @endif
@endsection