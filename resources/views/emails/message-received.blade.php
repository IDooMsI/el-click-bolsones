<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="card">
        <div class="card-body">
          <h3 class="card-title">Recibiste un mensaje de: {{$msg['nombre']}}</h3>
          <h3 class="card-subtitle mb-2 text-muted">Email: {{$msg['email']}}</h3>
          <p class="card-text">{{$msg['mensaje']}}</p>
        </div>
      </div>
</body>
</html>