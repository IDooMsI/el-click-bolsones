import ScrollReveal from 'scrollreveal';
window.onload = function() {
    ScrollReveal().reveal('.banner', { 
        duration: 3000,
        origin: 'bottom',
        distance: '-100px'
    });
    ScrollReveal().reveal('.bc1', { 
        delay: 1500,
        duration: 3000,
        origin: 'right',
        distance: '-50px'
    });
    ScrollReveal().reveal('.panelizq', { 
        duration: 3000,
        origin: 'right',
        distance: '-25px'
    });
    ScrollReveal().reveal('.panelder', { 
        duration: 3000,
        origin: 'left',
        distance: '-25px'
    });
    ScrollReveal().reveal('.title-form', { 
        duration: 3000,
        origin: 'bottom',
        distance: '-100px'
    });
    ScrollReveal().reveal('.title', { 
        duration: 3000,
        origin: 'bottom',
        distance: '-100px'
    });
    ScrollReveal().reveal('.sub-title', { 
        duration: 3000,
        origin: 'bottom',
        distance: '-100px'
    });
    ScrollReveal().reveal('.f-l', { 
        duration: 3000,
        origin: 'right',
        distance: '-25px'
    });
    ScrollReveal().reveal('.f-m', { 
        duration: 3000,
        origin: 'left',
        distance: '-25px'
    });
};