let banner = document.querySelector('#banner');
if (screen.width < 768) {
    console.log("Mobile");
    banner.setAttribute("src", "/storage/images/mobile/contentbanner.jpg");
}
else 
   if (screen.width < 1024) {
      console.log("Tablet"); 
      banner.setAttribute("src", "/storage/images/tablet/contentbannertablet.jpg");
}   
   else
    if (screen.width >= 1024) {
      console.log("Desktop");
      banner.setAttribute("src", "/storage/images/desktop/contentbannerdesk.jpg");
   }