<?php

namespace App\Http\Controllers;

use App\Bolson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BolsonController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bolson = Bolson::all()->last();
        $vac = compact('bolson');
        return view('admin.bolson.create',$vac);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Storage::delete('imagen-bolson-mes.jpg');
        Storage::disk('public')->delete('images/mobile/imagen-bolson-mes.png');
        Storage::disk('public')->delete('images/tablet/imagen-bolson-mes.png');
        Storage::disk('public')->delete('images/desktop/imagen-bolson-mes.png');

        $fileMobile = $request['mobile'];
        $nameMobile = 'imagen-bolson-mes' . "." . $fileMobile->extension();
        $pathMobile = $fileMobile->storeAs('images/mobile', $nameMobile, 'public');

        $fileTablet = $request['tablet'];
        $nameTablet = 'imagen-bolson-mes' . "." . $fileTablet->extension();
        $pathTablet = $fileTablet->storeAs('images/tablet', $nameTablet, 'public');

        $fileDesktop = $request['desktop'];
        $nameDesktop = 'imagen-bolson-mes' . "." . $fileDesktop->extension();
        $pathDesktop = $fileDesktop->storeAs('images/desktop', $nameDesktop, 'public');

        $bolson = Bolson::create([
            'image_mobile'  => $pathMobile,
            'image_tablet'  => $pathTablet,
            'image_desktop' => $pathDesktop
        ]);
        $vac = compact('bolson');
        return view('admin.index',$vac);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
