<?php

namespace App\Http\Controllers;

use App\Mail\MessageReceived;
use Illuminate\Support\Facades\Mail;

class MessagesController extends Controller
{
    public function store() {

        $msg = request();
        
        Mail::to('consumidores@elclickbolsones.com')->send(new MessageReceived($msg));

        $mensaje = "Mensaje Enviado";
        return $mensaje;
    }
}
