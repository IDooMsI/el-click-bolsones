# El click bolsones

### Caracteristicas
- Aplicacion: Laravel
- PHP 7
- HTML 5
- BOOTSTRAP 4

### Clonar
```bash
- git clone https://gitlab.com/IDooMsI/el-click-bolsones.git
```
### Si composer esta instalado global
```bash
- composer update
```
### Si no, descargar de aca
https://getcomposer.org/download/

### Instaplar npm (asegurarse tener instalado node primero) 
```bash
- npm install
```
```bash
- npm run dev
```

### Si no, descargar de aca y luego si instalar npm
https://nodejs.org/en/

### Key generate
```bash
- php artisan key:generate
```