<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('contactform');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/', 'MessagesController@store');
Route::post('send-email', 'MessagesController@store')->name('email');

Route::get('/pruebas', function () {

    return view('prueba-faqs');

});

Route::get('/admin','HomeController@admin')->name('admin');
Route::resource('bolson','BolsonController');

Auth::routes();