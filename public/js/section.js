
let $section = document.querySelector("#section__container");
let $i = 0
function closeSection(){
    $section.style.display = "none"  
}
window.addEventListener("load", () => {
    const BASE_URL = "http://191.232.170.254:81/api/sections.json?order[order]=asc";

    fetch(BASE_URL)
    .then(response => response.json())
    .then(result => {
        
        if(result[0] !== undefined){
            result.forEach((element) => {
                $section.style.display = "flex"
                $section.innerHTML += `

                <div id="accordionExample" class="accordion col-6">
                <div class="card ${$i%2 === 0 ? `panelizq` : `panelder` }">
                    <div class="card-header" id="heading${element.id}">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse${element.id}" aria-expanded="false" aria-controls="collapse${element.id}">
                                <h5 class="mb-0">
                                    ${element.icon === 'fa-plus' ? `<img class="img" src='{{ asset('storage/images/Iconos/Icono-01.svg') }}'>` : ""}
                                    ${element.icon === 'fa-question-circle' ? `<img class="img" src="{{ asset('storage/images/Iconos/Icono-02.svg') }}">` : ""}
                                    ${element.icon === 'fa-apple-alt' ? `<img class="img" src='{{ asset('storage/images/Iconos/Icono-03.svg') }}'>` : ""}
                                    ${element.icon === 'fa-map-marker-alt' ? `<img class="img" src='{{ asset('storage/images/Iconos/Icono-04.svg') }}'>` : ""}
                                    ${element.title} <i class="iconoflechafaq float-right fas fa-angle-right rotate-icon"></i>
                                </h5>
                            </button>   
                        </h2>
                    </div>
                    <div id="collapse${element.id}" class="collapse" aria-labelledby="heading${element.id}" data-parent="#accordionExample">
                        <div class="card-body">
                            ${element.content}
                        </div>
                    </div>
                </div>
                </div>
                `
                $i++
            });
        }
    })
    .catch(err => console.log(err))
   
})